package rocks.runur.dataclasses

data class Game (val tries: Int, val indicator: String, val guess: String)