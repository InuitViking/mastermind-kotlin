package rocks.runur.functions

import rocks.runur.dataclasses.Game
import kotlin.random.Random

class Functions {
	fun printGame(game: Game){
		println("Tries: ${game.tries}")
		println("Indicator: ${game.indicator}")
		println("Previous: ${game.guess}")
	}

	fun findElement(element: Int, arr: Array<Int>, size: Int): Boolean{
		for (i in 0..size){
			if(element == arr[i]){
				return true
			}
		}
		return false
	}

	fun generateCode(): MutableList<Int>{
		// Create an empty list
		val code: MutableList<Int> = mutableListOf()

		// Generate values and put them in the list
		for(i in 0..4) {
			// Even though code is "val", we somehow can still add values to it.
			// Also, Random number generation in kotlin is super easy
			code.add(Random.nextInt(1,6))
		}

		// Return the list
		return code
	}
}